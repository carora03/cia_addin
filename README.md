# README #

This README file explains the process to set up the Change Impact Analysis (CIA) plugins from requirements to SysML models.

### What is this repository for? ###

* This repository contains the code for an Enterprise Architect plugin for CIA of requirements to SysML models. It includes functions for both structural and behavioural models. 
* Additionally, the repository contains the jar file to compute the combined (syntactic and semantic) similarity of the keywords. The jar is in *NLP* folder. 
* The case study material has been provided *Example Models* folder. 

### How do I get set up? ###

* The EA plugin code can be set up by cloning this repository / downloading the source code, and following the steps provided [here](https://bellekens.com/2011/01/29/tutorial-create-your-first-c-enterprise-architect-addin-in-10-minutes/).
* The EA project provided in the repository contains a representative SysML example model. Once the EA plugin has been set up, the EA plugin can be executed using the steps mentioned in the CIA_FSE.pdf in the Example Models folder. The output files for the algorithm have been also added to this folder.
* The NLP jar is an executable jar file that can be executed as follows:

 1) *java -jar CIA_FSE.jar* --> This option can be used to compute the similarities for the default query keywords and EIS element keywords in the jar. These query keywords and EIS elements correspond to one of the 16 actual change-scenarios in our Delhi Cam Phase case study. The input and output files for the similarity calculation between query keywords and EIS element keywords have been added to the NLP folder.

2)  *java -jar CIA_FSE.jar <Path to Query Keywords> <Path to EIS Element keywords>* This option can be used to compute similarity between any query keywords and EIS elements keywords.