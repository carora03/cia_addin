﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows;
using System.Xml;
using System.Xml.Linq;

namespace CIA_AddIn
{
    public class CIA_AddInClass
    {
        // define menu constants
        const string menuHeader = "-&CIAAddin";
        const string menuHello = "&Say Hello";
        const string menuGoodbye = "&Say Goodbye";
        const string menuFindinAllDiagrams = "&Find in All Diagrams";
        const string menuDirectConnections = "&Get Direct Connections";
        const string menuOutPorts = "&Get All Out Ports";
        const string menuGetInfo = "&Get Information";
        const string menuAllTtransitiveConnections = "&Get Transitive Connections";
        const string menuAllTtransitiveConnections_includingComposite = "&Get Transitive Connections (structural)";
        const string menuAllTtransitiveConnections_includingBehavioral = "&Get Transitive Connections (include behavioral)";

        // remember if we have to say hello or goodbye
        private bool shouldWeSayHello = true;

        ///
        /// Called Before EA starts to check Add-In Exists
        /// Nothing is done here.
        /// This operation needs to exists for the addin to work
        ///
        /// <param name="Repository" />the EA repository
        /// a string
        public String EA_Connect(EA.Repository Repository)
        {
            //No special processing required.
            return "a string";
        }

        ///
        /// Called when user Clicks Add-Ins Menu item from within EA.
        /// Populates the Menu with our desired selections.
        /// Location can be "TreeView" "MainMenu" or "Diagram".
        ///
        /// <param name="Repository" />the repository
        /// <param name="Location" />the location of the menu
        /// <param name="MenuName" />the name of the menu
        ///
        public object EA_GetMenuItems(EA.Repository Repository, string Location, string MenuName)
        {

            switch (MenuName)
            {
                // defines the top level menu option
                case "":
                    return menuHeader;
                // defines the submenu options
                case menuHeader:
                    //string[] subMenus = { menuHello, menuGoodbye, menuFindinAllDiagrams, menuDirectConnections, menuOutPorts, menuGetInfo, menuAllTtransitiveConnections, menuAllTtransitiveConnections_includingComposite, menuAllTtransitiveConnections_includingBehavioral };
                    string[] subMenus = {menuAllTtransitiveConnections_includingComposite, menuAllTtransitiveConnections_includingBehavioral };
                    return subMenus;
            }

            return "";
        }

        ///
        /// returns true if a project is currently opened
        ///
        /// <param name="Repository" />the repository
        /// true if a project is opened in EA
        bool IsProjectOpen(EA.Repository Repository)
        {
            try
            {
                EA.Collection c = Repository.Models;
                return true;
            }
            catch
            {
                return false;
            }
        }

        ///
        /// Called once Menu has been opened to see what menu items should active.
        ///
        /// <param name="Repository" />the repository
        /// <param name="Location" />the location of the menu
        /// <param name="MenuName" />the name of the menu
        /// <param name="ItemName" />the name of the menu item
        /// <param name="IsEnabled" />boolean indicating whethe the menu item is enabled
        /// <param name="IsChecked" />boolean indicating whether the menu is checked
        public void EA_GetMenuState(EA.Repository Repository, string Location, string MenuName, string ItemName, ref bool IsEnabled, ref bool IsChecked)
        {
            if (IsProjectOpen(Repository))
            {
                switch (ItemName)
                {
                    // define the state of the hello menu option
                    case menuHello:
                        IsEnabled = shouldWeSayHello;
                        break;
                    // define the state of the goodbye menu option
                    case menuGoodbye:
                        IsEnabled = !shouldWeSayHello;
                        break;
                    case menuFindinAllDiagrams:
                        IsEnabled = true;
                        break;
                    case menuOutPorts:
                        IsEnabled = true;
                        break;
                    case menuDirectConnections:
                        IsEnabled = true;
                        break;
                    case menuGetInfo:
                        IsEnabled = true;
                        break;
                    case menuAllTtransitiveConnections:
                        IsEnabled = true;
                        break;
                    case menuAllTtransitiveConnections_includingComposite:
                        IsEnabled = true;
                        break;
                    case menuAllTtransitiveConnections_includingBehavioral:
                        IsEnabled = true;
                        break;
                    // there shouldn't be any other, but just in case disable it.
                    default:
                        IsEnabled = false;
                        break;
                }
            }
            else
            {
                // If no open project, disable all menu options
                IsEnabled = false;
            }
        }

        ///
        /// Called when user makes a selection in the menu.
        /// This is your main exit point to the rest of your Add-in
        ///
        /// <param name="Repository" />the repository
        /// <param name="Location" />the location of the menu
        /// <param name="MenuName" />the name of the menu
        /// <param name="ItemName" />the name of the selected menu item
        public void EA_MenuClick(EA.Repository Repository, string Location, string MenuName, string ItemName)
        {
            switch (ItemName)
            {
                // user has clicked the menuHello menu option
                case menuHello:
                    this.sayHello();
                    break;
                // user has clicked the menuGoodbye menu option
                case menuGoodbye:
                    this.sayGoodbye();
                    break;
                case menuFindinAllDiagrams:
                    this.findInAllDiagrams(Repository);
                    break;
                case menuOutPorts:
                    this.getOutPorts(Repository);
                    break;
                case menuDirectConnections:
                    this.getDirectlyconnectedElements(Repository);
                    break;
                case menuGetInfo:
                    GetInfo.getInfo(Repository);
                    break;
                case menuAllTtransitiveConnections:
                    this.getTransitiveClosure(Repository);
                    break;
                case menuAllTtransitiveConnections_includingComposite:
                    this.getTransitiveClosure_includeComposite(Repository);
                    break;
                case menuAllTtransitiveConnections_includingBehavioral:
                    getTransitiveClosure_includeBehaviour(Repository);
                    break;
            }
        }

        /// <summary>
        /// Function Find in All Diagrams
        /// 
        /// </summary>
        public HashSet<int> findInAllDiagrams(EA.Repository repository, EA.Element selected_elem)
        {
            HashSet<int> set_DiagIDs = new HashSet<int>();
            //EA.Package pkg = repository.GetPackageByID(dobj1.PackageID);
            //MessageBox.Show( " " + repository.GetContextItemType() + " " + repository.GetCurrentDiagram().DiagramID);
            String xmlStr = repository.SQLQuery("select dobj1.Diagram_ID from t_diagramobjects dobj1 where dobj1.object_id =" + selected_elem.ElementID + ";");
            //MessageBox.Show(xmlStr, "OK", MessageBoxButtons.OKCancel);
            //MessageBox.Show(selected_elem.MetaType + " " + selected_elem.EmbeddedElements.Count.ToString(), "OK", MessageBoxButtons.OKCancel);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlStr);
            StringBuilder sb = new StringBuilder();
            foreach (XmlElement row in doc.SelectNodes("//Row"))
            {
                sb.AppendLine(row.SelectSingleNode("Diagram_ID").InnerText);
                set_DiagIDs.Add(Int32.Parse(row.SelectSingleNode("Diagram_ID").InnerText));
                
            }
            //select * from t_diagramobjects dobj1, t_diagramobjects dobj2 where dobj1.object_id=dobj2.object_id and dobj1.diagram_id!=dobj2.diagram_id;
            return set_DiagIDs;
        }

        public HashSet<int> findInAllDiagrams(EA.Repository repository)
        {
            EA.Element selected_elem = (EA.Element)repository.GetContextObject();
            HashSet<int> set_DiagIDs = new HashSet<int>();
            //EA.Package pkg = repository.GetPackageByID(dobj1.PackageID);
            MessageBox.Show(" " + repository.GetContextItemType() + " " + repository.GetCurrentDiagram().DiagramID);
            String xmlStr = repository.SQLQuery("select dobj1.Diagram_ID from t_diagramobjects dobj1 where dobj1.object_id =" + selected_elem.ElementID + ";");
            MessageBox.Show(xmlStr, "OK", MessageBoxButtons.OKCancel);
            MessageBox.Show(selected_elem.MetaType + " " + selected_elem.EmbeddedElements.Count.ToString(), "OK", MessageBoxButtons.OKCancel);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xmlStr);
            StringBuilder sb = new StringBuilder();
            foreach (XmlElement row in doc.SelectNodes("//Row"))
            {
                sb.AppendLine(row.SelectSingleNode("Diagram_ID").InnerText);
                set_DiagIDs.Add(Int32.Parse(row.SelectSingleNode("Diagram_ID").InnerText));

            }
            MessageBox.Show(sb.ToString());
            //select * from t_diagramobjects dobj1, t_diagramobjects dobj2 where dobj1.object_id=dobj2.object_id and dobj1.diagram_id!=dobj2.diagram_id;
            return set_DiagIDs;
        }

        /// <summary>
        /// Function Find in All Diagrams
        /// 
        /// </summary>
        private void getOutPorts(EA.Repository repository)
        {
            EA.Element selected_elem = (EA.Element)repository.GetContextObject();
            //EA.Package pkg = repository.GetPackageByID(dobj1.PackageID);
           
            //select * from t_diagramobjects dobj1, t_diagramobjects dobj2 where dobj1.object_id=dobj2.object_id and dobj1.diagram_id!=dobj2.diagram_id;
            
            LinkedList<int> outPortIDs = Functions.getAllOutPorts(repository, selected_elem.ElementID);
            MessageBox.Show(Utilities.printList(repository, outPortIDs));
        }

        /// <summary>
        /// Function Find in All Diagrams
        /// 
        /// </summary>
        private void getDirectlyconnectedElements(EA.Repository repository)
        {
            EA.Element selected_elem = (EA.Element)repository.GetContextObject();
           


            //select * from t_diagramobjects dobj1, t_diagramobjects dobj2 where dobj1.object_id=dobj2.object_id and dobj1.diagram_id!=dobj2.diagram_id;
           
            LinkedList<int> directlyConnectedElements = Functions.getDirectlyConnectedElements(repository, selected_elem.ElementID);
            MessageBox.Show(Utilities.printList(repository, directlyConnectedElements));
            
        }

        /// <summary>
        /// Function Find in All Diagrams
        /// 
        /// </summary>
        private void getTransitiveClosure(EA.Repository repository)
        {
            EA.Element selected_elem = (EA.Element)repository.GetContextObject();
            
            //select * from t_diagramobjects dobj1, t_diagramobjects dobj2 where dobj1.object_id=dobj2.object_id and dobj1.diagram_id!=dobj2.diagram_id;
            TransitiveClosure closure = new TransitiveClosure();
            
            closure.getAllConnectedElement(repository, selected_elem.ElementID, "ROOT");
            Utilities.writeCSV(repository, TransitiveClosure.set_traversedElements, TransitiveClosure.breadcrums, "results");
            MessageBox.Show("File written");
        }

        /// <summary>
        /// Function Find in All Diagrams
        /// 
        /// </summary>
        private void getTransitiveClosure_includeComposite(EA.Repository repository)
        {
            EA.Element selected_elem = (EA.Element)repository.GetContextObject();

            //select * from t_diagramobjects dobj1, t_diagramobjects dobj2 where dobj1.object_id=dobj2.object_id and dobj1.diagram_id!=dobj2.diagram_id;
            TransitiveClosure_includeComposite closure = new TransitiveClosure_includeComposite();

            closure.getAllConnectedElement(repository, selected_elem.ElementID, "ROOT");
            Utilities.writeCSV(repository, TransitiveClosure_includeComposite.set_traversedElements, TransitiveClosure_includeComposite.breadcrums, selected_elem.Name + "_structural");
            MessageBox.Show("File written");
        }

        /// <summary>
        /// Function Find in All Diagrams
        /// 
        /// </summary>
        private void getTransitiveClosure_includeBehaviour(EA.Repository repository)
        {
            EA.Element selected_elem = (EA.Element)repository.GetContextObject();

            //select * from t_diagramobjects dobj1, t_diagramobjects dobj2 where dobj1.object_id=dobj2.object_id and dobj1.diagram_id!=dobj2.diagram_id;
            TransitiveClosure_includeBehaviour closure = new TransitiveClosure_includeBehaviour();

            closure.getAllConnectedElement(repository, selected_elem.ElementID, "ROOT");
            Utilities.writeCSV(repository, TransitiveClosure_includeBehaviour.set_traversedElements, TransitiveClosure_includeBehaviour.breadcrums, selected_elem.Name + "_behavioral");
            MessageBox.Show("File written");
        }

        private void getAllConnectedElements()
        {
            Tuple<int, int> tuple = new Tuple<int, int>(1,2);

        }

        ///
        /// Say Hello to the world
        ///
        private void sayHello()
        {
            MessageBox.Show("Hello World");
            this.shouldWeSayHello = false;
        }

        ///
        /// Say Goodbye to the world
        ///
        private void sayGoodbye()
        {
            MessageBox.Show("Goodbye World");
            this.shouldWeSayHello = true;
        }

        ///
        /// EA calls this operation when it exists. Can be used to do some cleanup work.
        ///
        public void EA_Disconnect()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

    }
}

