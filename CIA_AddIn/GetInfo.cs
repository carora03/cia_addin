﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIA_AddIn
{
    class GetInfo
    {

        public static void getInfo(EA.Repository repository)
        {
            StringBuilder sb = new StringBuilder();
            if (repository.GetContextItemType().Equals(EA.ObjectType.otElement))
            {
                EA.Element element = (EA.Element)repository.GetContextObject();
                sb.AppendLine("Name: " + element.Name);
                sb.AppendLine("Type: " + element.MetaType);
                sb.AppendLine("Id: " + element.ElementID);
                sb.AppendLine("Current Diagram: " + repository.GetCurrentDiagram().Name);
                sb.AppendLine("Current Package: " + repository.GetPackageByID(element.PackageID).Name);
            }
            else if (repository.GetContextItemType().Equals(EA.ObjectType.otDiagram))
            {
                EA.Diagram diag = (EA.Diagram)repository.GetContextObject();
                sb.AppendLine("Name: " + diag.Name);
                sb.AppendLine("Type: " + diag.MetaType);
                sb.AppendLine("Id: " + diag.DiagramID);
                sb.AppendLine("Current Package: " + repository.GetPackageByID(repository.GetPackageByID(diag.PackageID).PackageID).Name + "/" + repository.GetPackageByID(diag.PackageID).Name);
            }
            MessageBox.Show(sb.ToString());
        }
    }
}
