﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CIA_AddIn
{
    class Utilities
    {

        public static String printList(EA.Repository repository, ICollection<int> list)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int i in list)
            {
                EA.Element elem = repository.GetElementByID(i);
                sb.AppendLine(elem.Name + "," + elem.MetaType);
            }

            return sb.ToString();
        }

        private static String printDiags(EA.Repository repository, ICollection<int> list)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int i in list)
            {
                EA.Diagram elem = repository.GetDiagramByID(i);
                
                sb.Append(elem.Name + " ; ");
            }

            return sb.ToString();
        }

        public static void writeCSV(EA.Repository repository, ICollection<int> list, Dictionary<int, String> breadcrums, String filename)
        {
            StringBuilder sb = new StringBuilder();
            foreach (int i in list)
            {
                EA.Element elem = repository.GetElementByID(i);
                CIA_AddInClass addin = new CIA_AddInClass();
             
                sb.AppendLine(breadcrums[i] + "," + i + "," + elem.Name + "," + elem.MetaType + "," + printDiags(repository, addin.findInAllDiagrams(repository, elem)));
            }
            File.WriteAllText(".\\" + filename + ".csv", sb.ToString());
        }

        public static HashSet<int> getCompositeElements(EA.Repository repository, int id)
        {
            EA.Element elem = repository.GetElementByID(id);
            HashSet<int> diag_Objects = new HashSet<int>();

            if (elem.IsComposite)
            {
                EA.Diagram diag = elem.CompositeDiagram;
                foreach (EA.DiagramObject dObj in diag.DiagramObjects)
                {
                    diag_Objects.Add(dObj.ElementID);
                }
            }
            return diag_Objects;
        }

    }
}
