﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIA_AddIn
{
    class TransitiveClosure
    {
        public static HashSet<int> set_traversedElements;
        public static Dictionary<int, String> breadcrums;
        public TransitiveClosure()
        {
            set_traversedElements = new HashSet<int>();
            breadcrums = new Dictionary<int, string>();
        }
        
        public void getAllConnectedElement(EA.Repository repository, int elem_Id, String caller)
        {
            EA.Element element = repository.GetElementByID(elem_Id);
            String element_name = element.Name;
            String element_type = element.MetaType;
            caller = caller + ">>" + element_name;
            LinkedList<int> directlyConnectedElements = Functions.getDirectlyConnectedElements(repository, elem_Id);
            //Comment
            if (!set_traversedElements.Contains(elem_Id))
            {
                set_traversedElements.Add(elem_Id);
                breadcrums.Add(elem_Id, caller);
                
                if (element.MetaType.ToUpper().Equals("BLOCK") || element.MetaType.ToUpper().Equals("CLASS"))
                {
                    LinkedList<int> list_outPorts = Functions.getAllOutPorts(repository, elem_Id);
                    foreach (int port_id in list_outPorts)
                    {   
                        getAllConnectedElement(repository, port_id, caller);
                    }
                }
                else if (element.MetaType.ToUpper().Equals("PORT"))
                {
                    getAllConnectedElement(repository, element.ParentID, caller);
                }
            }

            
            foreach (int i in directlyConnectedElements)
            {
                if (!set_traversedElements.Contains(i))
                {
                    getAllConnectedElement(repository, i, caller);
                }           
            }
            return;
        }

    }
}
