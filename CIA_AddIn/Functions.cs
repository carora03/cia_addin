﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CIA_AddIn
{
    class Functions
    {

        /// <summary>
        /// Get all the directly connected Elements of an Element
        /// </summary>
        /// <returns></returns>
        public static LinkedList<int> getDirectlyConnectedElements(EA.Repository repository, int elem_Id)
        {
            LinkedList<int> returnList = new LinkedList<int>();
            EA.Element element = repository.GetElementByID(elem_Id);
            String[] notAllowedElements = { "NOTE"};
            foreach (EA.Connector connector in element.Connectors)
            {
                int source = connector.SupplierID;
                int target = connector.ClientID;
                int new_elem = (source == elem_Id) ? target : source;
                if (!returnList.Contains(new_elem))
                {
                    //Add only those elements which are matching the needed types
                    EA.Element new_Element = repository.GetElementByID(new_elem);
                    int pos = Array.IndexOf(notAllowedElements, new_Element.MetaType.ToUpper());
                    if (pos > -1)
                    {
                        continue;
                    }
                    else {
                        returnList.AddLast(new_Element.ElementID);
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Get all the directly connected Elements of an Element
        /// </summary>
        /// <returns></returns>
        public static LinkedList<int> getOutgoingConnectedElements(EA.Repository repository, int elem_Id)
        {
            LinkedList<int> returnList = new LinkedList<int>();
            EA.Element element = repository.GetElementByID(elem_Id);
            String[] notAllowedElements = { "NOTE" };
            foreach (EA.Connector connector in element.Connectors)
            {
                int source = connector.ClientID;
                int target = connector.SupplierID;
                int new_elem = (source == elem_Id) ? target : -1;
                if (!returnList.Contains(new_elem) && new_elem > -1)
                {
                    //Add only those elements which are matching the needed types
                    EA.Element new_Element = repository.GetElementByID(new_elem);
                    int pos = Array.IndexOf(notAllowedElements, new_Element.MetaType.ToUpper());
                    if (pos > -1)
                    {
                        continue;
                    }
                    else
                    {
                        returnList.AddLast(new_Element.ElementID);
                    }
                }
            }
            return returnList;
        }

        public static LinkedList<int> getAllOutPorts(EA.Repository repository, int block_Id)
        {
            LinkedList<int> returnList = new LinkedList<int>();
            try
            {
                EA.Element element = repository.GetElementByID(block_Id);
                foreach (EA.Element embeddedElement in element.EmbeddedElements)
                {
                    if (embeddedElement.MetaType.ToUpper().Contains("PORT"))
                    {
                        EA.TaggedValue val = embeddedElement.TaggedValues.GetByName("direction");
                        if (val != null && val.Value.Equals("out"))
                        {
                            returnList.AddLast(embeddedElement.ElementID);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return returnList;
        }
    }
}
